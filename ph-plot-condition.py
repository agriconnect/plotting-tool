#!/usr/bin/env python3

import gzip
import click
import pandas
import matplotlib
# Backend for Matplotlib. GTK3Agg is for Ubuntu. Should use Qt5Agg for non-Linux
matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt
from mpldatacursor import datacursor


@click.command()
@click.argument('filename', type=click.Path(exists=True))
@click.option('--style', type=click.Choice(('ggplot', 'seaborn')))
def main(filename, style):
    if not filename.endswith(('.csv', '.csv.gz')):
        click.echo('Not accepted format. Should be *.csv or *csv.gz')
        return
    if style == 'ggplot':
        plt.style.use('ggplot')
    elif style == 'seaborn':
        import seaborn
    # Read CSV and use first column ("Measured at") as index
    if filename.endswith('.gz'):
        open_func = gzip.open
    else:
        open_func = open
    with open_func(filename, 'r') as f:
        # Read first line to see how many column file has
        first_line = next(iter(f))
        total_columns = len(first_line.split(b','))
        # Reset file descriptor, to read from the beginning of the file
        f.seek(0)
        data = pandas.read_csv(f, index_col=0, usecols=range(total_columns - 1), parse_dates=True)
    # Another way to convert index to datetime type
    # data.index = pandas.to_datetime(data.index.values)
    column_names = tuple(data.columns)
    # If there are 2 columns to plot, we plot the 2nd to the right Y-axis
    axes = data.plot(secondary_y=column_names[1:])
    datacursor(axes)
    plt.show()


if __name__ == '__main__':
    main()
